---
title: Upgrade
summary: Upgrading LiberaForms
---

# Upgrading Euskara

We test the upgrade path thoroughly before each release but even so, you should always make a backup first!

Some releases require extra steps. Please [read the release notes](https://blog.liberaforms.org/category/releases.html) before you begin.

```
git pull origin main
source venv/bin/activate
pip install --upgrade pip
pip install -r ./requirements.txt
flask db upgrade
```

## Downgrading

You will need to [find the release](https://gitlab.com/liberaforms/liberaforms/-/releases) you want to downgrade to.

Let's say you want to downgrade from LiberaForms v3 to v2

```
git fetch origin tag v2.1.2 --no-tags
git checkout v2.1.2 -b v2.1.2
```

Check the version

```
cat VERSION.txt
```

`docs/INSTALL.md` now refers to the version you have checked out.

### venv

Delete `venv` and create it from scratch.

```
deactivate
rm -fr venv
python3 -m venv ./venv
source venv/bin/activate
pip install --upgrade pip
pip install -r ./requirements.txt
```

### Database

Restore the database from the backup you made that corresponds with the version.

```
sudo su
su postgres
cat <sql_dump_file> | psql -U postgres <db_name>
```

### Config files

Edit your `.env` file to match the expected values described in `dotenv.example`

Read `docs/INSTALL.md`

Check your nginx, supervisor, and gunicorn configurations to match.
