---
title: Jokabide-kodea
---

# LiberaForms-en Jokabide-kodea

> 1.0 bertsioa, 2021eko maiatzaren 12an berretsia.

LiberaForms [1] softwarean parte hartzen duten pertsonek proiektuaren gobernantza bi dokumentutan adostu dute: _Kontratu soziala_ [2] eta _Jokabide-kodea_. Dokumentu honetan Jokabide-kodea azaltzen da.

## Irizpideak, adibideak eta post-meritokrazia

### Irizpideak

LiberaForms proiektuan giro ireki eta atsegin bat sustatzeko, bertan parte hartzen dugunok konpromisoa hartzen dugu parte-hartze kualitatibo bat zaintzeko, zeinetan _Kontratu soziala_n zehaztutako printzipioak beteko diren, zeinetan ez den jazarpenik onartuko eta zeinetan edozein pertsona errespetatua sentituko den, dena dela ere bere adina, itxura fisikoa, dibertsitate funtzionala, etnia-, sexu- edo genero-identitatea, esperientzia-maila, zaletasun teknikoak, nazionalitatea, erlijioa edo estatus sozioekonomikoa.

Gauza batzuetan ados ez egotea erabat bateragarria da atsegina izan eta errespetuz aritzearekin. Horrez gain, gure ama-hizkuntza ez direnetan komunikatzea edo kultura-esperientzia oso ezberdinak edukitzea bezalako komunikazio-arazoak kontuan eduki behar ditugu, gaizki-ulertuak eta nahaste-borrasteak ekarri ditzaketelako. Eta elkarrizketek maiz beste hizketagaietan bukatzen dutenez, lantzen ari garen gaiari tinko eusten saiatu behar dugu. Gainera, beti da lagungarri modu argi, zehatz eta errespetuzkoan adieraztea.

Bestalde, hizketagai korapilatsuak alde batera utzita, oso onuragarria da laguntza eskatu edo gure zalantzak publikoki azaltzea, kolaborazioa eta zalantza berberak dituzten beste pertsonei soluzio bat aurkitzea errazteko.

### Giro atsegin bat sortzen lagundu dezaketen jokabideen adibideak

* Hizkera inklusibo eta atsegin bat erabiltzea.
* Beste ikuspuntu eta esperientziak errespetatzea.
* Besteengandik borondate ona espero izatea.
* Enpatia eta adeitasuna erakustea.
* Hizketagaiari eustea, zehatza, lankidetzakoa eta irekia izanez.
* Esaldi ironiko eta sarkastikoak erabiltzea ekiditea, gaizki-ulertuak ez egoteko.
* Arreta proiektuarentzat hoberena den horretan jarri.

### Jokabide onartezinen adibideak

* Proiektuaren testuinguruan hizkera edo iruditegi sexualitatea erabiltzea edota izan aurrez aurre, izan birtualki, limurtu nahian ibiltzea.
* Mezuen, profil-irudien edota erabiltzaile-izenen bidez norbait jazartzea, iraintzea, mespretxatzea edota eraso-pertsonalak egitea.
* Jazartzea edo beldurraraztea helburu duen edozein jokabide publiko edo pribatu.
* Haien baimen espliziturik gabe besteen datuak gehitzea, argitaratzea edota zabaltzea.
* Nahita gezurrak, laidoak edo bestelako desinformazioak argitaratzea edo zabaltzea.
* Elkarrizketa bat elikatzen jarraitzea edo besteak parte hartuaraztea, elkarrizketa utzi nahi dutela adierazi dutenean.
* Aipatutako guztiak, ironikoki edo bromatzat aurkeztuta bada ere.

### Deklarazio post-meritokratikoa

* Gure gizaki gisako balioa ez dago berez lotuta gure langile gisako balioari. Gure ogibideek ez gaituzte definitzen: egiten dugun lana baino askoz ere gehiago gara.
* Pertsonarteko abileziak, gutxienez, teknikoak bezain garrantzitsuak dira.
* Parte-hartzaile ez-teknikoen balioa teknikoena bezain garrantzitsua da.
* Esparru teknologikoan lan egitea pribilegioa da, ez eskubidea. Pertsona batzuen eragin kaltegarria ez da konpentsatzen haien ekarpen teknikoekin.
* Gure pribilegioak, gutxi badira ere, besteen bizitzak hobetzeko erabili nahi ditugu.
* Jendearen ongizatean kalteak eragiten dituen teknologietan parte hartzea errefusatzeko ardura partekatzen dugu.
* Edonolako pertsonen parte-hartzea erraztu nahi dugu, proiektuan integratu eta nahi dutena lortu dezaten. Horrek haiek gonbidatu eta babestu eta ahaldunduak sentitu daitezen esan nahi du.
* Profesional gisa balio gehiago ekartzen dugu gure identitateen, bizipenen eta ikuspuntuen aniztasuna kontuan hartzen badugu. Homogeneotasuna ez da jarraitu beharreko arau bat.
* Helburu hartzen dugun hori bizitza betegarri bat bizi bitartean burutu behar dugu. Gure arrakasta eta balioa ez dira soilik gure energia guztia softwarea garatzen erabiltzean oinarritzen.
* Gure printzipioak egiten dugun guztian islatzeen saiatzen gara. Printzipioak praktikan jarri gabe hauetaz hitz egitea ez da printzipioak edukitzea.

## Irismena, ondorioak eta ekintzak
### Irismena

Jokabide-kode hau proiektuko espazioetan eta proiektuak erabiltzen dituen beste espazio eta kanaletan aplikatzen da. Honek eztabaida foroak, txat-gelak, kode biltegiak, blogak eta Fedibertsoko kontuak barnebiltzen ditu, baita LiberaForms-ek eskainitako beste zerbitzuak, proiektuaren testuinguruko komunikazio pribatua eta LiberaForms-en izenean parte hartzen dugun beste edozein ekitaldi edo proiektu.

### Ondorioak

LiberaForms proiektuan parte hartzen duten pertsonengandik espero da onartezintzat jotako jokabideak eteteko eskatutakoan, berehala etetea. Norbaitek modu onartezinean jarduten badu, moderatzaileek beste lagunen erosotasuna eta segurtasuna mantentzeko beharrezkoa ikusten duten edozein ekintza hartzeko eskubidea edukiko dute, LiberaForms-en espazio eta kanaletatik behin-behinean edo betirako kanporatzea barne, edo pertsona horren jokabidea proiektuko beste kideekin edo publikoan partekatzea ere.

### Ekintzak

Norbait Jokabide-kode hau urratzen ari dela ikusten baduzu, egiten edo esaten ari dena desiragarria ez dela esan eta Jokabide-kode hau partekatu diezaiokezu, eta baita jokabide hori gelditu dezan eskatu.

Inor ez dela ari jokabide onartezin bat zuzentzen ikusten baduzu baina ez bazara nahikoa zure buruaz ziur sentitzen pertsona horri zuzenean hitz egiteko, jarri moderatzaile batekin harremanetan. Ahal bezain pronto moderatzaile batek horren ardura hartuko du. Edonola ere, garrantzizkoa da norbait salatzean modu pribatuan beharrezko informazioa gehitzea: estekak, pantaila-argazkiak, testuingurua edo egoera ulertu eta konpontzen lagundu lezakeen beste edozein informazio. Bidali informazioa moderatzaile bati.

Jarri moderatzaileekin harremanetan:

* [porru [@] liberaforms.org](mailto:porru@liberaforms.org). Euskera, Castellano, Esperanto, Català, English.
* [rita [@] liberaforms.org](mailto:rita@liberaforms.org). Català, Castellano, Français, Esperanto.
* [chris [@] liberaforms.org](mailto:chris@liberaforms.org). English, Castellano, Català.


## Kredituak
Dokumentu hau sortu, itzuli, adostu eta argitaratzeko software librea erabili dugu eta beste software libreko proiektu batzuetan inspiratu gara. Esker aunitz guzti horiei guretzako bidea urratzeagatik.

### Erabilitako softwarea

Eskerrak eman nahi dizkiegu softwareak garatzeagatik eta zerbitzuen inplementazio- eta mantentze-lanak egiteagatik, bereziki honelako zerbitzuak eskaintzen dituzten elkarte eta kolektiboei, hala nola, eXO [3] elkartearen Jitsiko bideokonferentziak eta LaLoka [4] eta Fedicat [5] kolektiboen pad-a, foroa, Mumble-a, hodeia, dokumentazio publikoa, webgunea eta sare-sozial digitalak. Erabilitako tresnak: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.

### Dokumentazioa
Jokabide-kode hau egiteko dokumentu hauetan oinarritu gara:

* [Debian-en Jokabide-kodea](https://www.debian.org/code_of_conduct)
* [Funkwhale-en Jokabide-kodea](https://funkwhale.audio/en_GB/code-of-conduct)
* [Lemmy-ren Jokabide-kodea](https://join.lemmy.ml/docs/en/code_of_conduct.html)
* [The Post-Meritocracy Manifesto](https://postmeritocracy.org/)

### Dokumentu honen lizentzia
LiberaForms-en Jokabide-kodearen lizentzia Creative Commons Aitortu Partekatu-berdin da, CC BY-SA. Honek esan nahi du dokumentu hau erabili eta moldatu dezakezula, beti ere egilea aipatzen baduzu eta lizentzia berdina erabiltzen baduzu aldaketarik eginez gero.

### Aipu-adibideak:
Nola aipatu dokumentu hau:

* Aldaketarik ez badago:  
> LiberaForms-en Jokabide-kodea \<https://docs.liberaforms.org/eu/etiketo>. Lizentzia: CC BY-SA

* Aldaketarik baldin badago:  
> Lan hau (aldaketak egin dituenaren izena)-(r)en moldaketa bat da. Jatorrizko lana: LiberaForms-en Jokabide-kodea \<https://docs.liberaforms.org/eu/etiketo>. Lizentzia: CC BY-SA

______
**Erreferentziak**:  

[1] [LiberaForms proiektua](https://liberaforms.org)  
[2] [Kontratu soziala](./social-contract.eu.md)  
[3] [eXO elkartea](https://exo.cat)   
[4] [LaLoka elkartea](https://laloka.org)   
[5] [Fedicat proiektua](https://fedi.cat)  
