---
title: Contrato Social
---

# Contrato Social de LiberaForms

> Versión 1.0 ratificada el 05 de mayo de 2021.

Las personas implicadas en el software LiberaForms [1] hemos acordado la gobernanza del proyecto en dos documentos: un *Contrato Social* y un *Código de Conducta*. En este documento, se expone el Contrato Social.

## Nuestro compromiso

LiberaForms empezó por una sencilla razón: crear un programa para confeccionar formularios que fuese fácil de usar y que se guiara por la descentralización web y el diseño ético [2]. Y esta es la misión principal del proyecto, que se materializa en:

* Desarrollar y mantener el software LiberaForms.
* Crear un programa que pueda ser instalado en un servidor y que se adapte a las necesidades de las personas que lo usan.
* Proveer de una experiencia descentralizada y no-publicitaria a personas que quieran crear formularios con tecnologías libres y de diseño ético.
* Explorar y trabajar maneras de hacer sostenibles la creación de cultura libre y bienes comunes digitales.

## Contrato Social con la comunidad del software libre

0. **Crearemos código y contenidos libres**  
LiberaForms es un software bajo licencia AGPLv3 [3] y posterior, y seguirá las recomendaciones de Reuse [4]. Los contenidos serán también siempre libres, bajo licencia CC BY-SA [5] o compatibles. Cuando escribamos nuevos componentes o creemos materiales relacionados, lo haremos con licencias libres.  
Crearemos el mejor programa que podamos, y tanto el código como la documentación serán públicos. Además, permitiremos que otras hagan uso de los recursos generados sin pedir ninguna retribución por nuestra parte.

1. **Sin publicidad**  
LiberaForms quiere ser sostenible a través de servicios ofrecidos a las personas y entidades que quieran hacer un uso personalizado y/o intensivo. LiberaForms no recibe ni quiere recibir dinero de fondos de inversión ni pretende ser una empresa tipo "start-up". Rechazando el modelo publicitario, nos comprometemos a desarrollar software libre bajo principios de diseño ético y sin anuncios.

2. **Priorizamos acciones teniendo en mente a las usuarias, la comunidad y la cultura libre**  
Nos guiaremos por las necesidades de nuestras usuarias y de la comunidad del software libre, bajo los principios de la cultura libre que posibilitan bienes comunes digitales. Y priorizaremos nuestras acciones en relación a esas necesidades y principios. Para hacerlo, proporcionaremos espacios públicos y privados de participación que se regirán por un Código de Conducta [6].

3. **Impulsaremos la soberanía tecnológica**  
Desde LiberaForms defenderemos los derechos digitales de las personas, y promoveremos la soberanía tecnológica y la descentralización web.

## Créditos

Para crear, traducir, acordar y publicar este documento hemos usado software libre y nos hemos inspirado en otros proyectos de la comunidad del software libre. A todas y todos, gracias por allanarnos el camino.

### Software usado

Queremos agradecer el desarrollo de software y la implementación y mantenimiento de servicios, especialmente a las asociaciones y colectivos que facilitan servicios como las videoconferencias en el Jitsi de la eXO [7] y el pad, el foro, el Mumble, la nube, la documentación pública, la web o los medios sociales de LaLoka [8] y Fedicat [9]. Herramientas usadas: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.

### Documentación

Para confeccionar este contrato social nos hemos inspirado en los siguientes documentos:

* [Contrato Social de Debian](https://www.debian.org/social_contract)
* [Principios de Write.as](https://write.as/principles)

### Licencia del documento

La licencia del contrato social de LiberaForms es Creative Commons Attribution Share-Alike, CC BY-SA. Es decir, puedes usar y modificar este documento siempre y cuando cites quién lo ha hecho y pongas la misma licencia si haces alguna modificación.

#### Ejemplos de cita

Cómo citar este documento:

* Si no hay modificación:
> Contrato Social de LiberaForms \<https://docs.liberaforms.org/es/socia-kontrakto.html>. Licencia: CC BY-SA

* Si hay modificación:
> Esta obra es una modificación de (y aquí quien modifica). Obra original: Contrato Social de LiberaForms \<https://docs.liberaforms.org/es/socia-kontrakto.html>. Licencia: CC BY-SA

____
**Referencias**:  
[1] [Proyecto LiberaForms](https://liberaforms.org/es)  
[2] [Principios del Diseño ético](https://2017.ind.ie/ethical-design/)  
[3] [Licencia AGPL](https://es.wikipedia.org/wiki/GNU_Affero_General_Public_License)  
[4] [Reuse Software](https://reuse.software/)  
[5] [Licencia CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.es)  
[6] [Código de conducta de LiberaForms](./code-of-conduct.es.md)  
[7] [Asociación eXO](https://exo.cat)  
[8] [Asociación LaLoka](https://laloka.org)  
[9] [Proyecto Fedicat](https://fedi.cat)  
