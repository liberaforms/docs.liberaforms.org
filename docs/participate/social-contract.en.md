---
title: Social Contract
---

# LiberaForms' Social Contract
> Version 1.0 ratified on May 05, 2021.

The people involved in the software LiberaForms [1] have agreed the project's governance into two documents: a *Social Contract* and a *Code of Conduct*. In this document the Social Contract is explained.

## Our commitment
LiberaForms appeared for a simple reason: create a software for creating forms that would be easy to use, guided by principles of the decentralization of the web and ethical design [2]. These are the project's main goals, which becomes a reality by:
* Developing and maintaining the software LiberaForms.
* Creating a software that can be installed on a server and that adapts to the needs of people using it.
* Offering a decentralized and a advertisement free experience to people who want to create forms with free technologies and ethical design.
* Researching and implementing a sustainable way of creating free culture and digital commons.

## Social Contract with the Free software community
0. **We will create free code and contents**   
LiberaForms is a software licensed under AGPLv3 [3] and later, and it will follow the recommendations from Reuse [4]. Also the contents will always be free, under CC BY-SA [5] or compatible licenses. When writing new components for the software LiberaForms or when creating related contents, we will do it under free licenses.   
We will create the best software we can, and both code and documentation of the project will be public. Besides that, we will allow others making use of resources generated without asking for reward for us.

1. **Without advertisement**  
LiberaForms wants to be sustainable through the services offered to people or entities who want to make custom and/or intensive use. LiberaForms does not receive, neither wants so, money from investment funds, nor is intended to be a "start-up" company. Rejecting the advertising model, we promise to develop Free software under principles of ethical design and without advertisement.

2. **We will prioritize actions having users, community and free culture in mind**   
We will be guided by the needs of our users and the Free software community under principles of free culture that make digital commons possible. We will prioritize our actions in relation to these needs and principles. To do so, we will provide public and private spaces for participation that will be governed by the Code of Conduct [6].

3. **We will promote technological sovereignty**  
LiberaForms will defend people's digital rights, promote technological self-governance and the decentralization of the web.

## Credits
For creating, translating, coming into agreement and publishing this document we used Free software and we were inspired by other Free software projects. Thanks to all of them to pave the way for us.

### Software used
We want to thank the development of the software and implementation and maintenance of the services, specially to associations and collectives that provide services such as the videoconferences on the Jitsi from eXO [7] and the pad, the forum, the Mumble, the cloud, the public documentation, the website or the social media from LaLoka [8] and Fedicat [9]. Tools used: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.

### Documentation
For making this social contract, we were inspired by these following documents:
* [Debian's Social Contract](https://www.debian.org/social_contract)
* [Write.as Principles](https://write.as/principles)

### License of this document
The license of the Liberaform's Contract Social is Creative Commons Attribution Share-Alike, CC BY-SA. This means that you can use and modify this document as long as mention the author and you use the same license if you make any modification.

### Examples of mention:
How to mention this document:

* If there is no modification:  
> Social Contract from LiberaForms \<https://docs.liberaforms.org/en/socia-kontrakto>. License: CC BY-SA

* If there is any modification:  
> This work is a modification by (the name of who modified it). Original work: Social Contract from LiberaForms \<https://docs.liberaforms.org/en/socia-kontrakto>. License: CC BY-SA

____
**References**:  
[1] [LiberaForms Project](https://liberaforms.org/en)  
[2] [Ethical Design principles](https://2017.ind.ie/ethical-design/)  
[3] [AGPL License](https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License)  
[4] [Reuse Software](https://reuse.software/)  
[5] [CC BY-SA License](https://creativecommons.org/licenses/by-sa/4.0/deed.en)  
[6] [LiberaForms' Code of Conduct](./code-of-conduct.en.md)  
[7] [Association eXO](https://exo.cat)  
[8] [Association LaLoka](https://laloka.org)  
[9] [Project Fedicat](https://fedi.cat)  
