---
title: Código de Conducta
---

# Código de Conducta de LiberaForms

> Versión 1.0 ratificada el 12 de mayo de 2021.

Las personas implicadas en el software LiberaForms [1] hemos acordado la governanza del proyecto en dos documentos: un _Contrato Social_ [2] y un _Código de Conducta_. En este documento exposemos el Código de conducta.

## Criterios, ejemplos y post-meritocracia

### Criterios

Para fomentar un ambiente abierto y acojedor en el proyecto LiberaForms, las personas que participamos nos comprometemos a velar por una participación cualitativa donde se respeten los principios del proyecto detallados en el *Contrato Social* y donde no tenga cabida ningún acoso y toda persona se sienta respetada, independientemente de su edad, apariencia física, diversidad funcional, identidad étnica, sexual o de género, nivel de experiencia, preferencies técnicas, nacionalidad, religión o estatus socioeconómico.

Estar en desacuerdo en algunas cosas es totalmente compatible con ser amables y respectuosas. Además, tenemos que tener en cuenta problemas comunicativos que pueden conllevar malentendidos o confusiones, como el hecho de comunicar en idiomas que no son nativos o tener bagajes culturales muy diferentes. Y, como a menudo una conversación deriva en otros temas, tenemos que intentar ser hábiles para ceñirnos al tema en cuestión. Además siempre es de ayuda expresarse de forma respetuosa, clara y concisa.

Asímismo, a excepción de temas sensibles, es muy positivo pedir ayuda o exponer dudas de forma pública para facilitar la colaboración y que otras personas que tengan las mismas dudas puedan encontrar la solución.

### Ejemplos de comportamientos que contribuyen a crear un ambiente agradable

* Usar un lenguaje inclusivo y acogedor.
* Respetar otros puntos de vista y experiencias.
* Presuponer buena voluntad.
* Mostrar empatía y amabilidad.
* Ceñirse a la temática haciendo gala de concisión, colaboración y apertura.
* Evitar usar frases irónicas o sarcáticas para que no haya malentendidos.
* Focalizar en lo que es mejor para el proyecto.

### Ejemplos de comportamientos inaceptables

* Usar lenguage o imaginario sexualizado y/o insinuarse tanto física como virtualmente en el contexto del proyecto.
* Molestar, insultar, hacer comentarios despectivos y/o ataques personales en mensajes, avatares y/o nombres de usuaria.
* Cualquier comportamiento público o privado que tenga por intención la persecución, el acoso o la intimidación.
* Agregar, publicar y/o diseminar datos de otras sin su permiso explícito.
* Publicar o difundir intencionadamente calumnias, injurias u otra desinformación.
* Continuar alimentando una conversación o incitar a otras a participar en ella después que una persona haya pedido salir de la conversación.
* Todo lo que se menciona, incluso si es presentado irónicamente o como una broma.

### Declaración post-meritocrática

* Nuestro valor como seres humanos no va intrínsecamente ligado a nuestro valor como trabajadoras. Nuestras profesiones no nos definen: somos mucho más que el trabajo que hacemos.
* Las competencias interpersonales son, como mínimo, tan importantes como las competencias técnicas.
* El valor de las contribuidoras no técnicas es igual de importante que el valor de las contribuidoras técnicas.
* Trabajar en el mundo tecnológico es un privilegio, no un derecho. El impacto negativo de algunas personas no se compensa con sus contribuciones técnicas.
* Queremos usar nuestros privilegios, por pocos que sean, para mejorar la vida de las demás personas.
* Compartimos la responsabilidad de rechazar participar en tecnologías que impactan negativamente en el bienestar de las personas.
* Queremos facilitar la participación de todo tipo de personas para que se puedan integrar y conseguir lo que se propongan. Eso significa invitarlas y que se sientan apoyadas y empoderadas.
* Aportamos más valor como profesionales si tenemos en cuenta la diversidad de nuestras identidades, experiencias y perspectivas. La homogeneidad no es un patrón a seguir.
* Tenemos que conseguir todo aquello que nos proponemos mientras vivimos una vida plena. Nuestro éxito y valor no se basan exclusivamente en el hecho de gastar toda nuestra energía en el desarrollo de software.
* Nos esforzamos para reflejar nuestros valores en todo lo que hacemos. Hablar de valores sin ponerlos en práctica no es tener valores.

## Alcance, consecuencias y acciones

### Alcance

Este Código de Conducta se aplica tanto en los propios espacios del proyecto como en otros espacios y canales donde el proyecto se implique. Esto incluye foros de discusión, salas conversacionales, repositorios de código, blogs y cuentas fediversales, así como otros servicios propuestos por LiberaForms, comunicaciones privadas en el contexto del proyecto y cualquier evento o proyecto en el que se esté participando en nombre del proyecto LiberaForms.

### Consecuencias

Se espera de las personas que participan en el proyecto LiberaForms que, si se les pide parar un comportamiento que se considera inaceptable, lo paren inmediatamente. Si una persona se comporta de forma inaceptable, las moderadoras podrán tomar cualquier acción que crean conveniente para el confort y la protección del resto de participantes, incluyendo la expulsión temporal o permanente de los espacios y canales de LiberaForms y, incluso, la puesta en conocimiento del comportamiento de esa persona a otros miembros del proyecto o de forma pública.

### Acciones

Si ves que alguien está infringiendo el Código de Conducta, puedes informar a la persona que lo que está haciendo o diciendo no es deseable enlazando a este Código de Conducta y, incluso, puedes pedirle que pare de hacer o decir tal cosa.

Si ves que nadie está corregiendo un comportamiento inaceptable pero no te sientes en confianza para dirigirte a la persona directamente, contacta con una moderadora. Tan pronto como sea posible, una moderadora se hará cargo. Eso sí, por favor, cuando reportes a alguien es importante incluir información relevante de forma privada: enlaces, capturas de pantalla, contexto u otras informaciones que puedan servir para entender y resolver la situación. Envia la información a una moderadora.

Contactar con las moderadoras:

* [rita [@] liberaforms.org](mailto:rita@liberaforms.org). Català, Castellano, Français, Esperanto.
* [chris [@] liberaforms.org](mailto:chris@liberaforms.org). English, Castellano, Català.
* [porru [@] liberaforms.org](mailto:porru@liberaforms.org). Euskera, Castellano, Esperanto, Català, English.

## Créditos

Para crear, traducir, acordar y publicar este documento hemos usado software libre y nos hemos inspirado en otros proyectos de la comunidad del software libre. A todas y todos, gracias por allanarnos el camino.

### Software usado

Queremos agradecer el desarrollo de software y la implementación y mantenimiento de servicios, especialmente a las asociaciones y colectivos que facilitan servicios como las videoconferencias en el Jitsi de la eXO [3] y el pad, el foro, el Mumble, la nube, la documentación pública, la web o los medios sociales de LaLoka [4] y Fedicat [5]. Herramientas usadas: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.


### Documentación

Para confeccionar este contrato social nos hemos inspirado en los siguientes documentos:

* [Código de Conducta de Debian](https://www.debian.org/code_of_conduct)
* [Código de Conducta de Funkwhale](https://funkwhale.audio/en_GB/code-of-conduct)
* [Código de Conducta de Lemmy](https://join.lemmy.ml/docs/en/code_of_conduct.html)
* [The Post-Meritocracy Manifesto](https://postmeritocracy.org/)

### Licencia del documento

La licencia del contrato social de LiberaForms es Creative Commons Attribution Share-Alike, CC BY-SA. Es decir, puedes usar y modificar este documento siempre y cuando cites quién lo ha hecho y pongas la misma licencia si haces alguna modificación.

#### Ejemplos de cita

Cómo citar este documento:

* Si no hay modificación:
> Código de Conducta de LiberaForms \<https://docs.liberaforms.org/es/etiketo.html>. Licencia: CC BY-SA

* Si hay modificación:
> Esta obra es una modificación de (y aquí quien modifica). Obra original: Código de Conducta de LiberaForms \<https://docs.liberaforms.org/es/etiketo.html>. Licencia: CC BY-SA

_____
**Referencias**:  
[1] [Proyecto LiberaForms](https://liberaforms.org)  
[2] [Contrato Social](./social-contract.es.md)  
[3] [Asociación eXO](https://exo.cat)   
[4] [Asociación LaLoka](https://laloka.org)   
[5] [Proyecto Fedicat](https://fedi.cat)  
