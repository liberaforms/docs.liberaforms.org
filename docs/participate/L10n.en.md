---
title: Translating
---

# Translating LiberaForms

If you would like to help, **thank you very much!**

LiberaForms is made up of five different parts

1. The server
2. Data display component
3. GDPR wizard
4. Form templates
5. Form Builder

Each of these projects require translation.

> We encourage you to use Weblate to translate LiberaForms. You can see an overview [here](https://hosted.weblate.org/projects/liberaforms/glossary/).

## 1. The server

The server generates web pages and runs background tasks like sending emails.

<a href="https://hosted.weblate.org/engage/liberaforms/">
<img src="https://hosted.weblate.org/widget/liberaforms/server-liberaforms/multi-auto.svg" alt="Translation status" /></a>

This set of translations includes both:

1. **Normal interface text**, such as buttons, page titles and explanations
2. **Inline help**, concise documentation included in the interface

Use the Weblate page [here](https://hosted.weblate.org/projects/liberaforms/server-liberaforms/) to translate the server.

### Variables

We use the standard gettext format to include variables in the translation.  
**You must not translate variables, but leave them as they are.**

English: `Hello %(username)s!`  
Spanish: `¡Hola %(username)s!`  

The variable `%(username)s` is automatically replaced with another text, in this case the user's name.

We also use a non-standard syntax to convert a part of a sentence into an HTML link.

English: `If you want to help, please read our $$documentation about translating$$ to learn how.`    
Spanish: `Si quieres ayudar, lee nuestra $$documentación sobre cómo traducir$$.`  

The text between the `$$` and `$$` is converted to a link.

### Markdown files

There are also two Markdown files that need to be translated.

1. [The site blurb](https://gitlab.com/liberaforms/liberaforms/-/tree/chore/L10n/assets/blurb). This is the text that is displayed on the `/index` page by default.
2. [Wizard disclaimer](https://gitlab.com/liberaforms/liberaforms/-/tree/chore/L10n/assets/gdpr-wizard). Users are required to read this text before using the GDPR wizard.

To translate these files you can either:

* Create a Gitlab account and make a merge request to [this branch](https://gitlab.com/liberaforms/liberaforms/-/tree/chore/L10n)
* Send your translation by email to `L10n@liberaforms.org`


## 2. Data display component

This component is used in many places throughout the interface to display different types of data.

<a href="https://hosted.weblate.org/engage/liberaforms/">
<img src="https://hosted.weblate.org/widget/liberaforms/data-display-liberaforms/multi-auto.svg" alt="Translation status" /></a>

Use the Weblate page [here](https://hosted.weblate.org/projects/liberaforms/data-display-liberaforms/) to translate the Data display component.

### Variables
**You must not translate variables, but leave them as they are.**

English: `Displaying {filtered} of {total}`  
Spanish: `Mostrando {filtered} de {total}`  

The variables `{filtered}` and `{total}` are replaced with other texts, in this case with numbers.


## 3. GDPR wizard

This application helps users write a suitable text that complies with the General Data Protection Law (GDPR).

<a href="https://hosted.weblate.org/engage/liberaforms/">
<img src="https://hosted.weblate.org/widget/liberaforms/gdpr-wizard/multi-auto.svg" alt="Translation status" />
</a>

Use the Weblate page [here](https://hosted.weblate.org/projects/liberaforms/gdpr-wizard/) to translate the GDPR wizard.

### Legal jargon

The wizard includes some legal jargon. If in doubt, please contact `L10n@liberaforms.org`

## 4. Form templates

Users can create forms with these form templates.

<a href="https://hosted.weblate.org/engage/liberaforms/">
<img src="https://hosted.weblate.org/widget/liberaforms/form-templates-liberaforms/multi-auto.svg" alt="Translation status" /></a>

Use the Weblate page [here](https://hosted.weblate.org/projects/liberaforms/form-templates-liberaforms/) to translate the form templates.

## 5. Form Builder

Users assemble forms using this interface area.

<a href="https://hosted.weblate.org/engage/liberaforms/">
<img src="https://hosted.weblate.org/widget/liberaforms/formbuilder-liberaforms/multi-auto.svg" alt="Translation status" /></a>

Use the Weblate page [here](https://hosted.weblate.org/projects/liberaforms/formbuilder-liberaforms/) to translate the Form Builder.

---
## License

Your contribution is licensed under the `CC-BY-SA-4.0`.

## Thanks again

It is important to us to bring LiberaForms to as many people as possible in their native tonge.

For any questions or enquiries please get in touch via `L10n@liberaforms.org`
