---
title: Codi de Conducta
---

# Codi de Conducta de LiberaForms

> Versió 1.0 ratificada el 12 de maig de 2021.

Les persones implicades en el programari LiberaForms [1] hem acordat la governança del projecte en dos documents: un *Contracte Social* [2] i un *Codi de Conducta*. En aquest document exposem el Codi de conducta.

## Estàndards, exemples i postmeritocràcia

### Estàndards

Per fomentar un entorn obert i acollidor al projecte LiberaForms, les persones que en formem part ens comprometem a vetllar per una participació qualitativa, on es respectin els principis del projecte detallats al *Contracte Social*, i on no hi tingui cabuda cap assetjament i tota persona s'hi senti respectada, independentment de la seva edat, aparença física, diversitat funcional, identitat ètnica, sexual o de gènere, nivell d'experiència, preferències tècniques, nacionalitat, religió o estatus socioeconòmic.

Estar en desacord en algunes coses és totalment compatible amb ser amables i respectuoses. A més, hem de tenir en compte problemes comunicatius de base que poden comportar malentesos o confusions, com el fet de comunicar en idiomes que no són nadius o tenir bagatges culturals molt diferents. I com sovint, una conversa deriva en altres temes, hem d'intentar ser hàbils per cenyir-nos al tema en qüestió. I sempre hi ajuda expressar-se de forma respectuosa, clara i concisa.

Així mateix, a excepció de temes sensibles, és molt positiu demanar ajut o exposar dubtes de forma pública per facilitar la col·laboració i que altres persones que tinguin els mateixos dubtes puguin trobar-ne la solució.

### Exemples de comportaments que contribueixen a crear un ambient agradable

* Usar un llenguatge inclusiu i acollidor.
* Respectar diferents punts de vista i experiències.
* Pressuposar bona voluntat.
* Mostrar empatia i amabilitat.
* Cenyir-se a una temàtica fent gala de concisió, col·laboració i apertura.
* Evitar usar frases iròniques o sarcàstiques perquè hi hagi menys malentesos.
* Focalitzar en el que és millor per al projecte.

### Exemples de comportaments inacceptables

* Usar llenguatge o imatgeria sexualitzada i/o insinuar-se tant física com virtualment en el context del projecte.
* Molestar, insultar, fer comentaris despectius i/o atacs personals en missatges, avatars i/o noms d'usuària.
* Qualsevol comportament públic o privat que tingui per intenció la persecució, l'assetjament o la intimidació.
* Agregar, publicar i/o disseminar dades d'altri sense el seu permís explícit.
* Publicar o difondre intencionadament calúmnies, injúries o altra desinformació.
* Continuar alimentant una conversa o incitar a altres a participar-hi després que una persona hagi demanat sortir de la conversa.
* Tot això que es menciona, inclòs si és presentat irònicament o com a una broma.

### Declaració postmeritocràtica

* El nostre valor com a éssers humans no va intrínsecament lligat al nostre valor com a treballadores. Les nostres professions no ens defineixen: som molt més que la feina que fem.
* Les competències interpersonals són, com a mínim, tant importants com les competències tècniques.
* El valor de les contribuïdores no tècniques és igual d'important que el valor de les contribuïdores tècniques.
* Treballar en el món tecnològic és un privilegi, no un dret. L'impacte negatiu d'algunes persones no es compensa amb les seves contribucions tècniques.
* Volem usar els nostres privilegis, per pocs que siguin, per millorar la vida de les altres persones.
* Compartim la responsabilitat de rebutjar participar de tecnologies que impacten negativament en el benestar de les persones.
* Volem facilitar la participació de tot tipus de persones perquè es puguin integrar i aconseguir el que es proposin. Això vol dir, no només convidar-les si no també assegurar-se que se senten recolzades i empoderades.
* Aportem més valor com a professionals si tenim en compte la diversitat de les nostres identitats, experiències i perspectives. La homogeneïtat no és un patró a seguir.
* Hem d'aconseguir tot allò que ens proposem mentre vivim una vida plena. El nostre èxit i valor no es basen exclusivament en el fet de gastar tota la nostra energia en el desenvolupament de programari.
* Ens esforcem per reflectir els nostres valors a tot el que fem. Parlar de valors sense posar-los en pràctica no és tenir valors.

## Abast, conseqüències i accions

### Abast

Aquest Codi de Conducta s'aplica tant als propis espais del projecte com a altres espais i canals on el projecte s'impliqui. Això inclou àgores de discussió, sales conversacionals, repositoris de codi, blogs i comptes fediversals, així com altres serveis proposats per LiberaForms, comunicacions privades en el context del projecte i qualsevol esdeveniment o projecte en el que s'estigui participant en nom del projecte LiberaForms.

### Conseqüències

S'espera de les persones que participen al projecte LiberaForms que, si se'ls demana aturar un comportament que es consideri inacceptable, l'aturin immediatament. Si una persona es comporta de forma inacceptable, les moderadores podran emprendre qualsevol acció que creguin adient per al confort i la protecció de la resta de participants, incloent l'expulsió temporal o permanent dels espais i canals de LiberaForms i, fins i tot, la posada en coneixement del comportament d'aquesta persona a altres membres del projecte o de forma pública.

### Accions

Si veus que algú està infringint el Codi de Conducta, pots informar a la persona que el que està fent o dient no és desitjable enllaçant a aquest Codi de Conducta i, fins i tot, pots demanar-li que pari de fer o dir tal cosa.

Si veus que cap altra persona està corregint un comportament inacceptable però no et sents en confiança per adreçar-te a la persona directament, contacta una moderadora. Tant aviat com sigui possible, una moderadora se'n farà càrrec. Això sí, si us plau, quan reportis a algú és important incloure informació rellevant de forma privada: enllaços, captures de pantalla, context o altres informacions que puguin servir per entendre i resoldre la situació. Envia la informació a una moderadora.

Contactar les moderadores:

* [rita [@] liberaforms.org](mailto:rita@liberaforms.org). Català, Castellano, Français, Esperanto.
* [chris [@] liberaforms.org](mailto:chris@liberaforms.org). English, Castellano, Català.
* [porru [@] liberaforms.org](mailto:porru@liberaforms.org). Euskera, Castellano, Esperanto, Català, English.

## Crèdits

Per crear, traduir, acordar i publicar aquest document hem usat programari lliure i ens hem inspirat d'altres projectes de la comunitat del programari lliure. A totes i tots, gràcies per aplanar-nos el camí.

### Programari utilitzat

Volem agrair el desenvolupament de programes i la implementació i manteniment de serveis, especialment a les associacions i col·lectius que faciliten serveis com: les videoconferències al Jitsi de l'eXO [3], o el pad, l'àgora, el parla, el núvol, la documentació pública, la web o els mitjans socials de LaLoka [4] i Fedicat [5]. Tot seguit, llistem les eines usades per a crear, acordar i publicar aquest document: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.


### Documentació

* [Codi de Conducta de Debian](https://www.debian.org/code_of_conduct)
* [Codi de Conducta de Funkwhale](https://funkwhale.audio/en_GB/code-of-conduct)
* [Codi de Conducta de Lemmy](https://join.lemmy.ml/docs/en/code_of_conduct.html)
* [The Post-Meritocracy Manifesto](https://postmeritocracy.org/)

### Llicència

La llicència d'aquest document és Creative Commons BY attribution Share-Alike, CC BY-SA. És a dir, pots usar i modificar aquest document sempre i quan citis qui l'ha fet i posis la mateixa llicència si en fas una modificació.

#### Exemples de cita

Com citar aquest document:

* Si no hi ha modificació:
> Codi de Conducta de LiberaForms \<https://docs.liberaforms.org/ca/etiketo.html>. Llicència: CC BY-SA

* Si hi ha modificació:
> Aquesta obra és una modificació de (i aquí qui modifica). Obra original: Codi de Conducta de LiberaForms \<https://docs.liberaforms.org/ca/etiketo.html>. Llicència: CC BY-SA

_____
**Referències**:  
[1] [Projecte LiberaForms](https://liberaforms.org)  
[2] [Contracte social](./social-contract.ca.md)  
[3] [Associació eXO](https://exo.cat)  
[4] [Associació LaLoka](https://laloka.org)  
[5] [Projecte Fedicat](https://fedi.cat)  
