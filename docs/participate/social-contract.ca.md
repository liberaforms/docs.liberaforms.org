---
title: Contracte Social
---

# Contracte Social de LiberaForms

> Versió 1.0 ratificada el 05 de maig de 2021.

Les persones implicades en el programari LiberaForms [1] hem acordat la governança del projecte en dos documents: un *Contracte Social* i un *Codi de Conducta*. En aquest document, exposem el Contracte Social.

## El nostre compromís

LiberaForms va començar per una senzilla raó: crear un programa per confeccionar formularis que fos fàcil d'usar i que es guiés per la descentralització web i el disseny ètic [2]. I aquesta és la missió principal del projecte, que es materialitza en:

* Desenvolupar i mantenir el programari LiberaForms.
* Crear un programa que pugui ser instal·lat en un servidor i que s'adapti a les necessitats de les persones que l'usen.
* Proveir d'una experiència descentralitzada i no-publicitària a persones que volen crear formularis amb tecnologies lliures i disseny ètic.
* Explorar i treballar maneres de fer sostenibles la creació de cultura lliure i béns comuns digitals.

## Contracte Social amb la comunitat del programari lliure

0. **Crearem codi i continguts lliures**  
LiberaForms és un programari sota llicència AGPLv3 [3] i posterior, i seguirà les recomanacions de Reuse [4]. Els continguts seran també sempre lliures, sota llicència CC BY-SA [5] o compatibles. Quan escriguem nous components del programari LiberaForms o creem materials relacionats, ho farem amb llicències lliures.  
Crearem el millor programa que puguem, i tant el codi com la documentació del projecte seran públics. A més, permetrem que d'altres facin ús dels recursos generats sense demanar cap retribució per part nostra.

1. **Sense publicitat**  
LiberaForms vol ser sostenible a través de serveis oferts a les persones i entitats que en volen fer un ús personalitzat i/o intensiu. LiberaForms no rep ni vol rebre diners de fons d'inversió ni pretén ser una empresa tipus "start-up". Rebutjant el model publicitari, ens comprometem a desenvolupar un programari lliure sota principis de disseny ètic i sense anuncis.

2. **Prioritzarem accions en base a les usuàries, la comunitat i la cultura lliure**  
Ens guiarem per les necessitats de les nostres usuàries i de la comunitat del programari lliure sota, els principis de la cultura lliure que possibiliten béns comuns digitals. I prioritzarem les nostres accions en relació a aquestes necessitats i principis. Per fer-ho, proporcionarem espais públics i privats de participació que es regiran per un Codi de Conducta [6].

3. **Impulsarem la sobirania tecnològica**  
Des de LiberaForms defensarem els drets digitals de les persones, i promourem la sobirania tecnològica i la descentralització web.

## Crèdits

Per crear, traduir, acordar i publicar aquest document hem usat programari lliure i ens hem inspirat d'altres projectes de la comunitat del programari lliure. A totes i tots, gràcies per aplanar-nos el camí.

### Programari utilitzat

Volem agrair el desenvolupament de programes i la implementació i manteniment de serveis, especialment a les associacions i col·lectius que faciliten serveis com les videoconferències al Jitsi de l'eXO [7] i el pad, l'àgora, el parla, el núvol, la documentació pública, la web o els mitjans socials de LaLoka [8] i Fedicat [9]. Eines usades: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.

### Documentació

Per confeccionar aquest contracte social ens hem inspirat dels següents documents:

* [Contracte Social de Debian](https://www.debian.org/social_contract)
* [Principis de Write.as](https://write.as/principles)

### Llicència del document

La llicència del contracte social de LiberaForms és Creative Commons Attribution Share-Alike, CC BY-SA. És a dir, pots usar i modificar aquest document sempre i quan citis qui l'ha fet i posis la mateixa llicència si en fas una modificació.

#### Exemples de cita

Com citar aquest document:

* Si no hi ha modificació:
> Contracte Social de LiberaForms \<https://docs.liberaforms.org/ca/socia-kontrakto>. Llicència: CC BY-SA

* Si hi ha modificació:
> Aquesta obra és una modificació de (i aquí qui modifica). Obra original: Contracte Social de LiberaForms \<https://docs.liberaforms.org/ca/socia-kontrakto>. Llicència: CC BY-SA

_____
**Referències**:  
[1] [Projecte LiberaForms](https://liberaforms.org/ca)  
[2] [Principis del Disseny ètic](https://2017.ind.ie/ethical-design/)  
[3] [Llicència AGPL](https://ca.wikipedia.org/wiki/GNU_Affero_General_Public_License)  
[4] [Reuse Software](https://reuse.software/)  
[5] [Llicència CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.ca)  
[6] [Codi de conducta de LiberaForms](./code-of-conduct.ca.md)  
[7] [Associació eXO](https://exo.cat)  
[8] [Associació LaLoka](https://laloka.org)  
[9] [Projecte Fedicat](https://fedi.cat)  
