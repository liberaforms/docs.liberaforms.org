---
title: Code of conduct
---

# LiberaForms' Code of Conduct

> Version 1.0 ratified on May 12, 2021.

The people involved with the software LiberaForms [1] have agreed the project's governance into two documents: a _Social Contract_ [2] and a _Code of Conduct_. In this document we explain the Code of Conduct.

## Standards, examples and post-meritocracy

### Standards

For promoting an open and welcoming environment on the LiberaForms project, we, the people who participate in it, commit to look after a qualitative participation in which the principles of the project detailed in the _Social Contract_ will be respected and in which no harassment will have place and every person will feel respected, independently of their age, their physical appearance, functional diversity, ethnic, sexual or gender identity, level of experience, technical preferences, nationality, religion or socioeconomic status.

Disagreeing on some things is totally compatible with being kind and respectful. Besides, we need to take into account communicative problems that can bring misunderstandings and confusions, such as the fact of communicating in non-native languages or having very different cultural experiences.  And, as a conversation often drifts into other topics, we need to try to be skillful in sticking to the topic at hand. In addition, expressing ourselves in a respectful, clear and concise way is always helpful.

Moreover, except for sensitive topics, it is very positive asking for help or explaining our doubts in public for easing both collaboration and that other people having the same doubts can find a solution.

### Examples of behaviors that contribute to create a pleasant environment

* Using an inclusive and welcoming language.
* Respecting other points of view and experiences.
* Assuming goodwill.
* Showing empathy and kindness.
* Sticking to the topic being concise, collaborative and open.
* Avoiding using ironical and sarcastic sentences so there won't be misunderstandings.
* Focus on what is best for the project.

### Examples of unacceptable behaviors

* Using sexualized language or imagery and/or insinuating oneself both physically and virtually in the context of the project.
* Harassing, insulting, making derogatory comments and/or personal attacks on messages, avatars and/or usernames.
* Any public or private behavior intended as persecution, harassment or intimidation.
* Attaching, publishing and/or spreading other people's data without their explicit permission.
* Intentionally publishing or spreading libel, slander or other misinformation.
* Continuing to feed a conversation or inciting others to join in after a person has asked to leave the conversation.
* All mentioned, even if it is presented ironically or as a joke.

### Post-meritocratic declaration

* Our value as human beings is not inherently linked to our value as workers. Our professions do not define us: we are much more than the work we do.
* Interpersonal skills are, at least, as important as the technical ones.
* The value of non-technical contributors is as important as the value of technical ones.
* Working in the technological area is a privilege, not a right. The negative impact of some people is not balanced out with their technical contributions.
* We want to use our privileges, however few, to improve the lives of others.
* We share the responsibility to refuse to participate in technologies that impact negatively the well-being of people.
* We want to ease the participation of all kinds of people so that they can integrate themselves and achieve what they want. That means to invite them and that they feel supported and empowered.
* We bring more value as professionals if take into account the diversity of our identities, experiences and perspectives. Homogeneity is not a standard to follow.
* We need to achieve everything we set out to do while living a full life. Our success and value are not based exclusively on spending all our energy on developing software.
* We strive to reflect our principles in everything we do. Talking about principles without putting them into practice is not having principles.

## Scope, consequences and actions
### Scope

This Code of Conduct applies both in the project spaces and in other spaces and channels where the project is involved. This includes discussion forums, chatrooms, code repositories, blogs and Fediverse accounts, as other services proposed by LiberaForms, private communication in the context of the project and any events or projects in which we are taking part on behalf of LiberaForms.

### Consequences

It is expected from people participating in the project LiberaForms that, if asked to stop a behavior considered unacceptable, they stop it immediately. If a person behaves in an unacceptable way, the moderators will have the right to take any action that they deem appropriate for the comfort and the protection of the other participants, including temporary or permanent expulsion from the spaces and channels of LiberaForms and even sharing the behavior of that person to other members of the project or to the whole public.

### Actions

If you see that someone is violating this Code of Conduct, you can inform that person that what they are making or saying is undesirable by linking to this Code of Conduct and you can even ask them to stop that behavior.

If you see that no one is correcting an unacceptable behavior but you do not feel confident to talk to that person directly, contact a moderator. As soon as possible, a moderator will take care of it. Nonetheless, when reporting someone it is important to include relevant information in a private way: links, screenshots, context or other information that may serve to understand and solve the situation. Send the information to a moderator.

Contact the moderators:

* [chris [@] liberaforms.org](mailto:chris@liberaforms.org). English, Castellano, Català.
* [porru [@] liberaforms.org](mailto:porru@liberaforms.org). Euskera, Castellano, Esperanto, Català, English.
* [rita [@] liberaforms.org](mailto:rita@liberaforms.org). Català, Castellano, Français, Esperanto.

## Credits

For creating, translating, coming into agreement and publishing this document we used free software and we were inspired by other free software projects. Thanks to all of them to pave the way for us.

### Software used
We want to thank the development of the software and implementation and maintenance of the services, specially to associations and collectives that provide services such as the videoconferences on the Jitsi from eXO [3] and the pad, the forum, the Mumble, the cloud, the public documentation, the website or the social media from LaLoka [4] and Fedicat [5]. Tools used: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.


### Documentation
For making this code of conduct, we were inspired by these following documents:

* [Debian's Code of Conduct](https://www.debian.org/code_of_conduct)
* [Funkwhale's Code of Conduct](https://funkwhale.audio/en_GB/code-of-conduct)
* [Lemmy's Code of Conduct](https://join.lemmy.ml/docs/en/code_of_conduct.html)
* [The Post-Meritocracy Manifesto](https://postmeritocracy.org/)

### License of this document
The license of the Liberaform's Code of Conduct is Creative Commons Attribution Share-Alike, CC BY-SA. This means that you can use and modify this document as long as you mention the author and you use the same license if you make any modification.

### Examples of mention:
How to mention this document:

* If there is no modification:  
> Code of Conduct from LiberaForms \<https://docs.liberaforms.org/en/etiketo>. License: CC BY-SA

* If there is any modification:  
> This work is a modification by (the name of who modified it). Original work: Code of Conduct from LiberaForms \<https://docs.liberaforms.org/en/etiketo>. License: CC BY-SA

________
**References**:  
[1] [LiberaForms Project](https://liberaforms.org)  
[2] [Social Contract](./social-contract.en.md)  
[3] [Association eXO](https://exo.cat)  
[4] [Association LaLoka](https://laloka.org)  
[5] [Project Fedicat](https://fedi.cat)
