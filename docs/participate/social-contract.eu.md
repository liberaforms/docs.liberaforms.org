---
title: Kontratu soziala
---

# LiberaForms-en Kontratu soziala

> 1.0 bertsioa, 2021eko maiatzaren 5ean berretsia.

LiberaForms [1] softwarean parte hartzen duten pertsonek proiektuaren gobernantza bi dokumentutan adostu dute: _Kontratu soziala_ eta _Jokabide-kodea_. Dokumentu honetan Kontratu soziala azaltzen da.

## Gure konpromisoa

LiberaForms arrazoi soil batengatik sortu zen: erabilerrazak eta sarearen deszentralizazioak eta diseinu etikoak [2] gidatutako galdetegiak egiteko software bat sortzea. Eta hau da proiektuaren xede nagusia, honela bihurtzen dena errealitate:

* LiberaForms softwarea garatuz eta mantenduz.
* Zerbitzaritan instalagarria den eta bere erabiltzaileen beharretara egokituko litzatekeen software bat sortuz.
* Galdetegiak teknologia libreekin eta diseinu etikoz erabili nahi dituzten pertsonei esperientzia deszentralizatu eta publizitate gabeko bat eskainiz.
* Kultura librea eta ondasun amankomun digitalak sortzeko modu jasangarri bat ikertuz eta landuz.

## Software librearen komunitatearekin daukagun Kontratu soziala

0.  **Kode eta eduki libreak sortuko ditugu**  
LiberaForms AGPLv3 [3] edo ondoren lizentziapeko software librea da, eta Reuse [4] gidako gomendioak jarraituko ditu. Edukiak ere beti libreak izango dira, CC BY-SA [5] edo honekin bateragarriak diren lizentziapean. LiberaForms softwareerako elementu berriak idatzi edo erlazionatutako edukiak sortzean, lizentzia libre pean egingo dugu.  
Ahalik eta software onena sortuko dugu, eta hala kodea, nola proiektuaren dokumentazioa publikoak izango dira. Horrez gain, guk sortutako baliabideak beste batzuei erabiltzeko aukera emango diegu, trukean guretzako ordainsaririk eskatu gabe.

1. **Iragarkirik gabe**  
LiberaForms-ek erabilera pertsonalizatu edota intentsiboa egin nahi duten pertsona edo erakundeei eskainitako zerbitzuen bidez izan nahi du jasangarri. LiberaForms-ek ez du jasotzen, eta ez du jaso nahi ere, inbertsio-funtsetako dirurik, eta ez du asmorik "start-up" motako enpresa izateko ere. Publizitatean oinarritutako modeloa errefusatuz, diseinu etikoaren printzipiopeko eta iragarkirik gabeko software libre bat garatzea hitzematen dugu.

2. **Erabiltzaileak, komunitatea eta kultura librea buruan edukita lehenetsiko ditugu jarduerak**  
Gure erabiltzaileen eta software librearen komunitatearen beharrek gidatuko gaituzte, ondasun amankomun digitalak ahalbidetzen dituzten kultura librearen printzipioen pean. Horretarako, parte hartzeko espazio publiko eta pribatuak sortuko ditugu, Jokabide-kode [6] batez gidatutakoak.

3. **Teknologia-burujabetza sustatuko dugu**  
LiberaForms-ek jendearen eskubide digitalak defendatu eta teknologia-burujabetza eta sarearen deszentralizazioa sustatuko ditu.

## Kredituak
Dokumentu hau sortu, itzuli, adostu eta argitaratzeko software librea erabili dugu eta beste software libreko proiektu batzuetan inspiratu gara. Esker aunitz guzti horiei guretzako bidea urratzeagatik.

### Erabilitako softwarea

Eskerrak eman nahi dizkiegu softwareak garatzeagatik eta zerbitzuen inplementazio- eta mantentze-lanak egiteagatik, bereziki honelako zerbitzuak eskaintzen dituzten elkarte eta kolektiboei, hala nola, eXO [7] elkartearen Jitsiko bideokonferentziak eta LaLoka [8] eta Fedicat [9] kolektiboen pad-a, foroa, Mumble-a, hodeia, dokumentazio publikoa, webgunea eta sare-sozial digitalak. Erabilitako tresnak: GNU/Linux, LibreOffice, Pluma, Etherpad, Discourse, Jitsi, Mumble, Nextcloud, Arxius Oberts, Grav, Pleroma.

### Dokumentazioa
Kontratu sozial hau egiteko dokumentu hauetan oinarritu gara:

* [Debianen Kontratu soziala](https://www.debian.org/social_contract)
* [Write.as proiektuaren printzipioak](https://write.as/principles)

### Dokumentu honen lizentzia
LiberaForms-en Kontratu sozialaren lizentzia Creative Commons Aitortu Partekatu-berdin da, CC BY-SA. Honek esan nahi du dokumentu hau erabili eta moldatu dezakezula, beti ere egilea aipatzen baduzu eta lizentzia berdina erabiltzen baduzu aldaketarik eginez gero.

### Aipu-adibideak:
Nola aipatu dokumentu hau:

* Aldaketarik ez badago:  
> LiberaForms-en Kontratu soziala \<https://docs.liberaforms.org/eu/socia-kontrakto>. Lizentzia: CC BY-SA

* Aldaketarik baldin badago:  
> Lan hau (aldaketak egin dituenaren izena)-(r)en moldaketa bat da. Jatorrizko lana: LiberaForms-en Kontratu soziala \<https://docs.liberaforms.org/eu/socia-kontrakto>. Lizentzia: CC BY-SA

_____
**Erreferentziak**:  
[1] [LiberaForms proiektua](https://liberaforms.org/eu)  
[2] [Diseinu etikoaren printzipioak](https://2017.ind.ie/ethical-design/)  
[3] [AGPL lizentzia](https://eu.wikipedia.org/wiki/GNU_Affero_General_Public_License)  
[4] [Reuse Software gida](https://reuse.software/)  
[5] [CC BY-SA lizentzia](https://creativecommons.org/licenses/by-sa/4.0/deed.eu)  
[6] [LiberaForms-en Jokabide-kodea](./code-of-conduct.eu.md)  
[7] [eXO elkartea](https://exo.cat)  
[8] [LaLoka elkartea](https://laloka.org)  
[9] [Fedicat proiektua](https://fedi.cat)  
